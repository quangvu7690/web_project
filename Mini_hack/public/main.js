$(document).ready(function() {
    // var x = $('#scoreInput_0').val();
    // console.log(x);
    // var round = 0;
    var data_global = null;
    // var db = null;
    var dataLoaded = false;
    const scoreId = window.location.pathname.replace("/games/", "");
    //lấy dữ liệu từ api 
    var round = 0
    loadData(scoreId);
    //kết thúc lấy dữ liệu
    //click on button --> then change data    
    
    $('button').click(function() {
        round++;
        append(round);
        console.log(round);
    });
    
    $(document).on('input', 'input', function() {
        // let round = parseInt($(this).attr("round"));
        let position = $(this).attr('id');
        let positionSplit = position.split('_');
        let playerPosition = parseInt(positionSplit[2]);
        let round = parseInt(positionSplit[1]);    
        let arr = [];
        console.log($(this).val()+" "+playerPosition);
        console.log(round);
        
        arr.push(parseInt($(this).val()));
        arr = JSON.stringify(arr);
        
        for(let i = 0; i < 4; i++){
            $('#total-player-'+i).text(data_global[i].score[0]);
        };
        $('#total-score').text(total(data_global));
        $.ajax({
            url: "/asd/" + scoreId ,
            type: 'POST',
            data: {
                arr: arr,
                round: round,
                position: playerPosition
            },
            success: function(data) {
                loadData(scoreId);        
            }
        })
    });

    
    //cac ham su dung 
    function loadData(id) {
        $.ajax({
            type: "GET",
            url: "/api/games/" + id,
            success: function(DB) {
                // console.log(this);
                
                let data = DB.score.player;
                data_global = data;
                db = DB;
                round = getRound(data);
                if(data[0].score.length > 0 && dataLoaded == false){
                    for (let i = 1; i < data[0].score.length; i++){
                        append(i+1);
                    }
                    display(data);
                    dataLoaded = true;
                }
                else if(dataLoaded){
                    display(data);
                }
                else {
                    display(data);
                }
                // console.log(total(data));
            },
            error: function(err) {
                console.log(err);
            }
        });
    }
    // hien diem 
    function showScore(data){
        for (let x = 0; x < data.length; x++) {
            for (let y = 0; y < data[x].score.length; y++ )
                $('#scoreInput_'+y+'_'+x).val(data[x].score[y]);
        }
    }

    function getRound(data){
        return data[0].score.length;
    }

    function display(data){
        for (let i = 0; i < data.length; i++) {
            $('#name'+i).text(data[i].name);
        }
        showScore(data);
        for(let i = 0; i < data.length; i++){
            $('#total-player-'+i).text(playerScore(data, i));
        };
        $('#total-score').text(total(data_global));
    }

    //total score of 1 player 
    function playerScore(data, x){
        let sum = 0;
        for ( let i = 0; i < data[x].score.length; i++){
            sum += data[x].score[i];
        }
        return sum;
    }
    // tinh tong diem 
    function total(data) {
        // let arr = [a, b, c, d];
        let column_sum = 0;
        for( let x = 0; x < data.length; x++){
            for( let y = 0; y < data[x].score.length; y++){
                column_sum += data[x].score[y]; 
            }
        }
        return column_sum;
    }
    //append 
    function append(index){
        $("#myTable").append(`         
        <tr>
            <th scope="row">Round ${index}</th>
            <td><input id="scoreInput_${index - 1}_0" type="number" class="form-control mb-3" ></td>
            <td><input id="scoreInput_${index - 1}_1" type="number" class="form-control mb-3" ></td>
            <td><input id="scoreInput_${index - 1}_2" type="number" class="form-control mb-3" ></td>
            <td><input id="scoreInput_${index - 1}_3" type="number" class="form-control mb-3" ></td>
        </tr>
       `);
    };
});